from django.urls import path

from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('future/', views.tambahan, name='tambahan'),
    path('story1/', views.story1, name='story1'),
]