from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'alfa3.html')

def profile(request):
    return render(request, 'pagetwo.html')

def tambahan(request):
    return render(request, 'pagetambahan.html')

def story1(request):
    return render(request, 'PALF1.html')